/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 * Copyright (c) 2015 Runtime Inc
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include <errno.h>

#include <settings.h>
#include "settings/settings_file.h"

bool settings_subsys_initialized;

void settings_init(void);

int settings_backend_init(uint16_t sector_size, uint16_t sector_count, const char* file_path);

int settings_subsys_init(uint16_t sector_size, uint16_t sector_count, const char*  file_path)
{

	int err = 0;

	if (settings_subsys_initialized) {
		return 0;
	}

	settings_init();

	err = settings_backend_init(sector_size, sector_count, file_path);

	if (!err) {
		settings_subsys_initialized = true;
	}

	return err;
}
