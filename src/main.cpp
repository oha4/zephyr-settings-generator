/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#include "settingswriter.h"
#include <string_view>
#include <memory.h>
#include <settings.h>

#include <boost/program_options.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

namespace po = boost::program_options;

int main(int argc, char * argv[]){
  std::stringstream ss;
  ss << DESCRIPTION << std::endl << std::endl<< "Allowed options";
  po::options_description desc(ss.str());
  desc.add_options()
      ("help,h", "Print this help message")
      ("version", "Print version Information")
      ("settings", po::value< std::vector<std::string> >(), "Settings pairs in the format <key>:<type>:<value>. E.g. `wifi/ssid:string:My_Network`")
      ("output,o", po::value< std::string>()->default_value("settings.bin"), "Path for output file to generate")
      ("sector-count,c", po::value<size_t>()->default_value(8), "Number of sectors to generate. Default: 8")
      ("sector-size,s", po::value<size_t>()->default_value(4096), "Size of a single sector. Default: 4096")
  ;

  po::positional_options_description p;
  p.add("settings", -1);
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
          options(desc).positional(p).run(), vm);
  po::notify(vm);


  if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
  }

  if (vm.count("version")) {
      std::cout << VERSION_STR << std::endl;
      return 0;
  }

  if (vm.count("settings"))
  {
    settings_subsys_init(vm["sector-size"].as<size_t>(), vm["sector-count"].as<size_t>(), vm["output"].as<std::string>().c_str());
    auto settings_writer = [](std::string str){
      std::unique_ptr<SettingsWriter> writer = SettingsWriterFactory::create(str);
      writer->write();

    };
    auto settings = vm["settings"].as< std::vector<std::string> >();
    std::for_each(std::begin(settings), std::end(settings), settings_writer);
  }

  return 0;
}