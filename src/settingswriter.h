/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#ifndef SETTINGS_WRITER_H
#define SETTINGS_WRITER_H

#include <string_view>
#include <string>
#include <memory>

class SettingsWriter
{
public:
  virtual int write() const = 0;

protected:
  std::string_view get_key_from_string(std::string_view str) const;
  std::string_view get_type_from_string(std::string_view str) const;
  std::string_view get_value_from_string(std::string_view str) const;
};

class SettingsWriterFactory{
public:
  static std::unique_ptr<SettingsWriter> create(const std::string s);
};

class StringSettingsWriter : public SettingsWriter
{
public:
  StringSettingsWriter(const std::string data);
  ~StringSettingsWriter();

  int write() const override;

private:
  const std::string m_data;
};

class U32SettingsWriter : public SettingsWriter
{
public:
  U32SettingsWriter(const std::string data);
  ~U32SettingsWriter();

  int write() const override;

private:
  const std::string m_data;
};




#endif /* SETTINGS_WRITER_H */