/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#include "settingswriter.h"

#include <settings.h>

#include <iostream>

std::unique_ptr<SettingsWriter> SettingsWriterFactory::create(const std::string s){
  std::string substr{s.substr(s.find(':')+1)};
  std::string type{substr.substr(0, substr.find(':'))};

  if(type == "string") return std::move(std::make_unique<StringSettingsWriter>(s));
  if(type == "u32") return std::move(std::make_unique<U32SettingsWriter>(s));

  return std::unique_ptr<SettingsWriter>{nullptr};
}

std::string_view SettingsWriter::get_key_from_string(std::string_view str) const{
  std::string_view key{str.substr(0, str.find(':'))};

  return key;
}

std::string_view SettingsWriter::get_type_from_string(std::string_view str) const{
  std::string_view substr{str.substr(str.find(':')+1)};
  std::string_view type{substr.substr(0, substr.find(':'))};

  return type;
}

std::string_view SettingsWriter::get_value_from_string(std::string_view str) const{
  std::string_view substr{str.substr(str.find(':')+1)};
  std::string_view value{substr.substr(substr.find(':')+1)};

  return value;
}

StringSettingsWriter::StringSettingsWriter(const std::string data) : m_data(data){

}

StringSettingsWriter::~StringSettingsWriter(){

}

int StringSettingsWriter::write() const{
  std::string key{get_key_from_string(m_data)};
  std::string type{get_type_from_string(m_data)};

  if(type != "string") return -1;
  std::string value{get_value_from_string(m_data)};

  std::cout << "Store Setting \"" << key << "\" with value \"" << value << "\"" << std::endl;
  settings_save_one(key.c_str(), value.data(), value.size());

  return 0;
}

U32SettingsWriter::U32SettingsWriter(const std::string s) : m_data(s){

}

U32SettingsWriter::~U32SettingsWriter(){

}

int U32SettingsWriter::write() const{

  std::string key{get_key_from_string(m_data)};
  std::string type{get_type_from_string(m_data)};

  if(type != "u32") return -1;

  std::string value{get_value_from_string(m_data)};
  uint32_t val = std::stoi(value);

  std::cout << "Store Setting \"" << key << "\" with value \"" << value << "\"" << std::endl;
  settings_save_one(key.c_str(), &val, sizeof(val));

  return 0;
}
