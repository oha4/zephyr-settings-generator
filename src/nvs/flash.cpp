#include "flash.h"

#include <memory.h>
/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#include <iostream>
#include <algorithm>
#include <array>
#include <fstream>
#include <vector>
#include <iterator>
#include <exception>
#include <string>

struct device {

  device(const std::string path){
    fs = std::fstream(path,  std::ios::out | std::ios::in | std::ios::trunc | std::ios::binary);
    if(!fs.is_open()) throw std::runtime_error("Could not open file `binary_file.bin");
  }

  ~device(){
    fs.close();
  }

  uint32_t id;
  std::fstream fs;
};

struct device* flash_create(uint16_t sector_size, uint16_t sector_count, const char* file_path){
  static device dev(file_path);

  std::fill_n(std::ostream_iterator<char>(dev.fs), sector_size*sector_count, 0xFF);
  dev.fs.flush();
  dev.fs.seekp(0);
  return &dev;
}

int flash_write(const struct device * dev, off_t offset, const uint8_t* data, size_t len){

  // std::cout << "Write " << len << " bytes data at offset 0x" << std::hex << offset << std::endl;;
  // std::for_each(data, data+len, [](char x){std::cout << "0x" << std::hex << (unsigned)x << " ";});
  // std::cout << std::endl;

  struct device* _dev = const_cast<struct device*>(dev);

  _dev->fs.seekp(offset);
  _dev->fs.write((const char*)data, len);
  _dev->fs.flush();

  return 0;
}

int flash_read(const struct device * dev, off_t offset, void * data, size_t len){
  struct device* _dev = const_cast<struct device*>(dev);
  _dev->fs.seekg(offset);
  _dev->fs.read((char*)data, len);
  return 0;
}

int flash_erase(const struct device * dev, off_t offset, size_t size){
  struct device* _dev = const_cast<struct device*>(dev);
  std::vector<uint8_t> tmp(size);
  std::generate_n(tmp.begin(), size, []{return 0xFF;});
  _dev->fs.seekp(offset);
  _dev->fs.write((const char*)tmp.data(), tmp.size());
  return 0;
}