/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#ifndef FLASH_H_
#define FLASH_H_

#include <sys/types.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct device;

struct device* flash_create(uint16_t sector_size, uint16_t sector_count, const char* file_path);
int flash_write(const struct device * dev, off_t offset, const uint8_t* data, size_t len);
int flash_read(const struct device * dev, off_t offset, void * data, size_t len);
int flash_erase(const struct device * dev, off_t offset, size_t size);

#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* FLASH_H_ */