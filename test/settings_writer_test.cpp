/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#include "settings_save_mock.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <settingswriter.h>

MATCHER(IsStringSettingsWriter, "")
{
  if (arg == NULL)
  {
    return false;
  }
  StringSettingsWriter* derived = dynamic_cast<StringSettingsWriter*>(arg);
  if (derived == NULL)
  {
    *result_listener << "is NOT a StringSettingsWriter*";
    return false;
  }
  return true;
}

MATCHER(IsU32SettingsWriter, "")
{
  if (arg == NULL)
  {
    return false;
  }
  U32SettingsWriter* derived = dynamic_cast<U32SettingsWriter*>(arg);
  if (derived == NULL)
  {
    *result_listener << "is NOT a U32SettingsWriter*";
    return false;
  }
  return true;
}

TEST(Factory, create_string_writer){
  std::unique_ptr<SettingsWriter> writer = SettingsWriterFactory::create("wifi/ssid:string:my-ssid");
  EXPECT_THAT(writer.get(), IsStringSettingsWriter());
}

TEST(Factory, create_u32_writer){
  std::unique_ptr<SettingsWriter> writer = SettingsWriterFactory::create("test/intval:u32:123456");
  EXPECT_THAT(writer.get(), IsU32SettingsWriter());
}

TEST(StringSettingsWriterTest, extract_name){
  StringSettingsWriter s("wifi/ssid:string:my-ssid");
  s.write();
  EXPECT_THAT(Settings::name(), testing::StrEq("wifi/ssid"));
}

TEST(StringSettingsWriterTest, extract_value){
  StringSettingsWriter s("wifi/ssid:string:my-ssid");
  s.write();
  std::string str(reinterpret_cast<const char*>(Settings::value().data()));
  EXPECT_THAT(str, testing::StrEq("my-ssid"));
}

TEST(StringSettingsWriterTest, fails_if_settings_datatype_not_string){
  StringSettingsWriter s("wifi/ssid:u32:my-ssid");
  int ret = s.write();
  EXPECT_THAT(ret, testing::Ne(0));
}

TEST(U32SettingsWriterTest, extract_value){
  U32SettingsWriter s("wifi/ssid:u32:123456");
  s.write();
  uint32_t val(*reinterpret_cast<const uint32_t*>(Settings::value().data()));
  EXPECT_THAT(val, 123456);
}