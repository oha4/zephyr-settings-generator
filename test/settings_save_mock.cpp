/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#include "settings_save_mock.h"

#include <cstddef>
#include <string>
#include <vector>

#include <memory.h>

static std::string g_name;
static std::vector<uint8_t> g_value;

extern "C" {
int settings_save_one(const char *name, const void *value, size_t val_len){
  g_name = std::string(name);
  g_value.resize(val_len);
  memcpy(g_value.data(), value, val_len);

  return 0;
}
}

std::string Settings::name(){
  return g_name;
}

const std::vector<uint8_t> Settings::value(){
  return g_value;
}