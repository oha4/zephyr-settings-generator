/*
 * SPDX-FileCopyrightText: 2023 Albert Krenz <albert.krenz@mailbox.org>
 * SPDX-License-Identifier: BSD-2-Clause-Patent
*/

#ifndef SETTINGS_SAVE_MOCK_H
#define SETTINGS_SAVE_MOCK_H

#include <stddef.h>
#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

int settings_save_one(const char *name, const void *value, size_t val_len);
#ifdef __cplusplus
}
#endif

namespace Settings{
  std::string name();
  const std::vector<uint8_t> value();
}


#endif /* SETTINGS_SAVE_MOCK_H */